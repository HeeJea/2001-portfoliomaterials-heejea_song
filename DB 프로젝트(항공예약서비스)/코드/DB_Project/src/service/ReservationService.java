package service;

import java.util.List;

import domain.Airline.AirlineTicket;
import domain.Airline.Airplane;
import domain.Airline.Airplaneroute;
import domain.Customer.CustomerSeat;
import domain.Customer.Myreservation;
import domain.Customer.Reservation;

public interface ReservationService {

	Reservation deleteuser(String customerId);
	Reservation selectuser(String customerId);
	List<Airplaneroute> Reservationlist(String startPoint, String endPoint);
	List<AirlineTicket> MyReservationlist(String ID);
	Airplaneroute Reservation(String routeno);
	String seat(String seatno);
	List<CustomerSeat> seatall();
	Myreservation mylist(String seatnumber, String routeno);
	Myreservation myflist(Myreservation mylist, String customerid, String airnumber);
	Myreservation mypage(String customerid, String seatNumber);
	Myreservation tomypage(String userid);
	Myreservation remypage(String reservationnumber);
	Airplaneroute airtemp(String routeno);
	Airplane airplane(String airname, String time);
	
	Myreservation myreservationid(String reservationno);
	List<Myreservation> myreadlist(String userid);
	Myreservation remove(String reservationno);

}
