package service.logic;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import domain.Airline.AirlineTicket;
import domain.Airline.Airplane;
import domain.Airline.Airplaneroute;
import domain.Customer.CustomerSeat;
import domain.Customer.Myreservation;
import domain.Customer.Reservation;
import service.ReservationService;
import store.ReservationStore;

@Service
public class ReservationServiceLogic implements ReservationService{

	
	@Autowired
	private ReservationStore store;
	
	
	@Override
	public Reservation deleteuser(String customerId) {
		return store.readdeleteuser(customerId);
	}


	@Override
	public Reservation selectuser(String customerId) {
		// TODO Auto-generated method stub
		return store.readselectuser(customerId);
	}


	@Override
	public List<Airplaneroute> Reservationlist(String startPoint, String endPoint) {
		return store.ReservationList(startPoint, endPoint);
	}


	@Override
	public List<AirlineTicket> MyReservationlist(String ID) {
		
		return store.MyReservationList(ID);
	}


	@Override
	public Airplaneroute Reservation(String routeno) {
		return store.reservation(routeno);
	}


	@Override
	public String seat(String seatno) {
		// TODO Auto-generated method stub
		return store.seat(seatno);
	}

	@Override
	public List<CustomerSeat> seatall() {
		// TODO Auto-generated method stub
		return store.readseatall();
	}


	@Override
	public Myreservation mylist(String seatnumber, String routeno) {
		// TODO Auto-generated method stub
		return store.readmylist(seatnumber,routeno);
	}


	@Override
	public Myreservation myflist(Myreservation mylist, String customerid, String airnumber) {
		// TODO Auto-generated method stub
		return store.readmyflist(mylist, customerid, airnumber);
	}


	@Override
	public Myreservation mypage(String customerid, String seatNumber) {
		// TODO Auto-generated method stub
		return store.mypage(customerid, seatNumber);
	}


	@Override
	public Airplaneroute airtemp(String routeno) {
		// TODO Auto-generated method stub
		return store.readairtemp(routeno);
	}


	@Override
	public 	Airplane airplane(String airname, String time){
		// TODO Auto-generated method stub
		return store.readairplane(airname, time);
	}


	@Override
	public Myreservation tomypage(String userid) {
		// TODO Auto-generated method stub
		return store.readtomypage(userid);
	}


	@Override
	public Myreservation remypage(String reservationnumber) {
		// TODO Auto-generated method stub
		return store.readremypage(reservationnumber);
	}


	@Override
	public List<Myreservation> myreadlist(String userid) {
		// TODO Auto-generated method stub
		return store.readmyreadlist(userid);
	}


	@Override
	public Myreservation remove(String reservationno) {
		// TODO Auto-generated method stub
		return store.readremove(reservationno);
	}


	@Override
	public Myreservation myreservationid(String reservationno) {
		// TODO Auto-generated method stub
		return store.readmyreservationid(reservationno);
	}

}
