package domain.Airline;

public class Airplane {

	private String airplaneNumber; // 비행기 번호
	private String airplaneModel; // 비행기 기종
	private String airplaneName;
	private String flyingState; // 운항 여부 
	private String boardTime; // 탑승 시간  
	
	public String getAirplaneNumber() {
		return airplaneNumber;
	}
	public void setAirplaneNumber(String airplaneNumber) {
		this.airplaneNumber = airplaneNumber;
	}
	public String getAirplaneModel() {
		return airplaneModel;
	}
	public void setAirplaneModel(String airplaneModel) {
		this.airplaneModel = airplaneModel;
	}
	public String getAirplaneName() {
		return airplaneName;
	}
	public void setAirplaneName(String airplaneName) {
		this.airplaneName = airplaneName;
	}
	public String getFlyingState() {
		return flyingState;
	}
	public void setFlyingState(String flyingState) {
		this.flyingState = flyingState;
	}
	public String getBoardTime() {
		return boardTime;
	}
	public void setBoardTime(String boardTime) {
		this.boardTime = boardTime;
	}
	
}
