package domain.Airline;

public class Airplaneroute {

	private String airplanerouteNumber;
	private String airlineName;
	private String startPoint;
	private String endPoint;
	private String startTime;
	private String endTime;
	private String wayPoint;
	private String airplaneNumber;
	
	public String getAirplanerouteNumber() {
		return airplanerouteNumber;
	}
	public void setAirplanerouteNumber(String airplanerouteNumber) {
		this.airplanerouteNumber = airplanerouteNumber;
	}
	public String getAirlineName() {
		return airlineName;
	}
	public void setAirlineName(String airlineName) {
		this.airlineName = airlineName;
	}
	public String getStartPoint() {
		return startPoint;
	}
	public void setStartPoint(String startPoint) {
		this.startPoint = startPoint;
	}
	public String getEndPoint() {
		return endPoint;
	}
	public void setEndPoint(String endPoint) {
		this.endPoint = endPoint;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public String getWayPoint() {
		return wayPoint;
	}
	public void setWayPoint(String wayPoint) {
		this.wayPoint = wayPoint;
	}
	public String getAirplaneNumber() {
		return airplaneNumber;
	}
	public void setAirplaneNumber(String airplaneNumber) {
		this.airplaneNumber = airplaneNumber;
	}
	
	
}
