package domain.Customer;

public class Reservation {

	private String customerId;
	private String customerName;
	private String airlineTicketId;
	private String vairlineName;
	private String startPoint;
	private String endPoint;
	private String boardTime;
	private String seatNumber;
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getAirlineTicketId() {
		return airlineTicketId;
	}
	public void setAirlineTicketId(String airlineTicketId) {
		this.airlineTicketId = airlineTicketId;
	}
	public String getVairlineName() {
		return vairlineName;
	}
	public void setVairlineName(String vairlineName) {
		this.vairlineName = vairlineName;
	}
	public String getStartPoint() {
		return startPoint;
	}
	public void setStartPoint(String startPoint) {
		this.startPoint = startPoint;
	}
	public String getEndPoint() {
		return endPoint;
	}
	public void setEndPoint(String endPoint) {
		this.endPoint = endPoint;
	}
	public String getBoardTime() {
		return boardTime;
	}
	public void setBoardTime(String boardTime) {
		this.boardTime = boardTime;
	}
	public String getSeatNumber() {
		return seatNumber;
	}
	public void setSeatNumber(String seatNumber) {
		this.seatNumber = seatNumber;
	}
	
}
