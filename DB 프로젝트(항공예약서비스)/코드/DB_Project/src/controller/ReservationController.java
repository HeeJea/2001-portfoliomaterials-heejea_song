package controller;

import java.util.List;

import javax.servlet.http.HttpSession;
import javax.swing.JOptionPane;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import domain.Airline.AirlineTicket;
import domain.Airline.Airplane;
import domain.Airline.Airplaneroute;
import domain.Customer.CustomerInformation;
import domain.Customer.CustomerSeat;
import domain.Customer.Myreservation;
import domain.Customer.Reservation;
import service.CustomerService;
import service.ReservationService;

@Controller
@RequestMapping("reservation")
public class ReservationController {

	@Autowired
	private CustomerService Cservice;
	
	@Autowired
	private ReservationService service;
		
	@RequestMapping("mypage.do")
	public String mypage(Model model, String userid) {
		Myreservation my =  service.tomypage(userid);
		model.addAttribute("mylist", my);
		return "myReservation";
	}
	
	
	@RequestMapping("myReservation.do")
	public String mylist(HttpSession session, Model model, String reservationno) {

		System.out.println(reservationno);
		Myreservation my =  service.remypage(reservationno);
		model.addAttribute("mylist", my);
		return "myReservation";
	}
	
	@RequestMapping(value = "reservationlist.do", method = RequestMethod.POST)
	   public String Airlinereservation(HttpSession session, Model model, String startPoint, String endPoint) {
	      
      List<Airplaneroute> reservationList = service.Reservationlist(startPoint, endPoint);
	      model.addAttribute("reservation", reservationList);
	      return "Reservation";
	   }
	
	@RequestMapping("Myreservationlist")
	public String Myreservationlist(HttpSession session,Model model) {
		CustomerInformation customer1 = (CustomerInformation) session.getAttribute("loginedUser");
	      
	      if (customer1 == null) {
	         return "login";
	      }
	      
	      List<AirlineTicket> myreservationlist = service.MyReservationlist(customer1.getCustomerId());
	      model.addAttribute("myreservationlist",myreservationlist);
	      
		return "mypage";
	}
	
	@RequestMapping(value="userreservation.do", method = RequestMethod.POST)
	   public String UserReservation(String routeno, CustomerSeat seat, HttpSession session) {
	      Airplaneroute air = new Airplaneroute();
	     
	      air = service.Reservation(routeno);
	      String seatno = service.seat(seat.getSeatNumber());
	      System.out.print(routeno);
	      System.out.print(seatno);
	      CustomerInformation customer = (CustomerInformation) session.getAttribute("loginedUser");
	      String userid = customer.getCustomerId();
	      
	      Cservice.insertReservation(air,seatno,userid);
	      return "reservationt:/reservation/Myreservationlist.do";
	   }

	
	@RequestMapping("seat.do")
	public String Reservationseat(HttpSession session, Model model, String routeno) {
		 
		  String customerid = (String) session.getAttribute("customerid");
	      if (customerid == null) {
	    	  JOptionPane.showMessageDialog(null, "회원 정보가 없습니다.");
	    	  return "login";
	      }
			
		  Airplaneroute air = new Airplaneroute();
	      air = service.Reservation(routeno);
	      model.addAttribute("route",air.getAirplanerouteNumber());
	      
	      List<CustomerSeat> clist = service.seatall();
	      
	      System.out.println(clist.size());
	      
	      model.addAttribute("seatclass", clist);
	      
	      model.addAttribute("route",air);
		
		return "seatselect";
	}
	
	@RequestMapping("reservationfinal.do")
	public String reservationfinal(HttpSession session, Model model, String routeno, String seatNumber) {
		 
		String customerid = (String) session.getAttribute("customerid");
		Airplaneroute route = service.airtemp(routeno);
		String airname = route.getAirlineName();
		String time = route.getStartTime();

		Airplane air = service.airplane(airname, time);
		String airnumber = air.getAirplaneNumber();
		
		Myreservation my = service.mylist(seatNumber, routeno);
		System.out.println(my.getAirlinename());
		Myreservation nmy = service.myflist(my, customerid, airnumber);
        model.addAttribute("mylist", nmy);
        
        Myreservation temp =  service.mypage(customerid, seatNumber);
        String reservationnumber = temp.getReservationno();
        
		return "redirect:myReservation.do?reservationno=" + reservationnumber;
	}

}