<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%> 
<!DOCTYPE html>
<html lang="ko">
<head>
<title>마이페이지</title>
 <%@ include file="/views/common.jsp"%>
</head>
<body>
<div style="padding:100px">
<h2>MY 페이지</h2>
<hr>
<br>
<div align="center">
<form action="${pageContext.request.contextPath}/customer/join.do" method="post">
		<table class="table">
			<tr>
				<th>ID :</th>
				<td>${user.customerId}</td>
			</tr>
			<tr>
				<th>비밀번호 :</th>
				<td>${user.customerPass}</td>
			</tr>
			<tr>
				<th>이름 :</th>
				<td>${user.customerName}</td>
			</tr>
			<tr>
				<th>나이 :</th>
				<td>${user.customerAge}</td>
			</tr>
			<tr>
				<th>성별 :</th>
				<td>${user.customerSex}</td>
			</tr>
			<tr>
				<th>전화번호 :</th>
				<td>${user.customerNumber}</td>
			</tr>
			<tr>
				<th>주민번호:</th>
				<td>${user.customerSSNumber}</td>
			</tr>
			<tr>
				<th>여권번호 :</th>
				<td>${user.passportNumber}</td>
			</tr>
			<tr>
				<th>주소 :</th>
				<td>${user.customeraddr}</td>
			</tr>
			
		</table><br>
		<div align="center">
			<a href = "${pageContext.request.contextPath}/customer/myrelist.do?userid=${user.customerId}"><input class="btn btn-success" type="button" value="나의 예약리스트 확인하기"></a>
		<a href="${pageContext.request.contextPath}/customer/modify.do"><input type="button" class="btn btn-primary" value="수정"></a>
		<a href = "${pageContext.request.contextPath}/customer/userdelete.do"><input class="btn btn-success" type="button" value="회원탈퇴"></a>
		
		</div>
	</form>
</div>
</div>
</body>
</html>