<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%> 
<!DOCTYPE html>
<html lang="ko">
<head>
<title>예약리스트 확인</title>
 <%@ include file="/views/common.jsp"%>

</head>
<body>
<div style="padding:100px">
<h2>노선정보</h2>
<hr>
<br>
<div align="center">

      <table class="table">
         <colgroup>
           <col width="200"/>
           <col width="200"/>
           <col width="200"/>
           <col width="200"/>
           <col width="200"/>
           <col width="200"/>
           <col width="100"/>
           <col width="100"/>
          </colgroup>
          <thead>
           <tr>
            <th class="text-center">노선번호</th>
            <th class="text-center">항공사</th>
            <th class="text-center">출발시간</th>
            <th class="text-center">도착시간</th>
            <th class="text-center">출발지</th>
            <th class="text-center">도착지</th> 
            <th class="text-center">예약</th>
           </tr>
          </thead>
         <tbody>
          <c:choose>
           <c:when test="${empty reservation}">
          <tr>
            <th colspan="8" class="text-center">노선이없습니다.</th>
          </tr>
           </c:when>
           <c:otherwise>
            <c:forEach var="res" items="${reservation}">
          <tr>
           <td class="text-center">${res.airplanerouteNumber}</td>
           <td class="text-center">${res.airlineName}</td>
           <td class="text-center">${res.startTime}</td>
           <td class="text-center">${res.endTime}</td>
           <td class="text-center">${res.startPoint}</td>
           <td class="text-center">${res.endPoint}</td>
           <td class="text-center">
              <a href = "${pageContext.request.contextPath}/reservation/seat.do?routeno=${res.airplanerouteNumber}"><input class="btn btn-success" type="button" value="예약"></a>
           </td>
          </tr>
          </c:forEach>
           </c:otherwise>
          </c:choose>
         </tbody>
         
      </table><br>
   
</div>


</div>

		<section id="footer">
			<div class="inner">
				<form method="post" action="#">
					<div class="footer">
						<ul>
							<li><a href="#">사이트 도움말</a></li>
							<li><a href="#">사이트 이용약관</a></li>
							<li><a href="#">사이트 운영규칙</a></li>
							<li><a href="#"><strong>개인정보취급방침</strong></a></li>
							<li><a href="#">책임의 한계와 법적고지</a></li>
							<li><a href="#">게시중단요청 서비스</a></li>
							<li><a href="#">고객센터</a></li>
						</ul>
					</div>
				</form>
				<div class="copyright">
					&copy; BaekSeokUnivercity : DataBaseMasterTeam</a>.
				</div>
			</div>
		</section>
</body>
</html>