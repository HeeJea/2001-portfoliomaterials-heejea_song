<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ko">
<style type="text/css">
body {
	padding-top: 100px;
	padding-bottom: 40px;
	background-color: #efefef;
}

.login-header {
	max-width: 400px;
	padding: 15px 29px 25px;
	margin: 0 auto;
	background-color: #ffbb00;
	color: white;
	text-align: center;
	-webkit-border-radius: 15px 15px 0px 0px;
	-moz-border-radius: 15px 15px 0px 0px;
	border-radius: 15px 15px 0px 0px;
	-webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
	-moz-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
	box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
}

.login-footer {
	max-width: 400px;
	margin: 0 auto 20px;
	padding-left: 10px;
}

.form-signin {
	max-width: 400px;
	padding: 29px;
	margin: 0 auto 20px;
	background-color: #fcf2f2;
	-webkit-border-radius: 0px 0px 15px 15px;
	-moz-border-radius: 0px 0px 15px 15px;
	border-radius: 0px 0px 15px 15px;
	-webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
	-moz-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
	box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
}

.form-signin .form-signin-heading, .form-signin .checkbox {
	margin-bottom: 15px;
}

.form-signin input[type="text"], .form-signin input[type="password"] {
	font-size: 16px;
	height: auto;
	margin-bottom: 15px;
	padding: 7px 9px;
}

.form-btn {
	text-align: center;
	padding-top: 20px;
}

.form-control {
	display: block;
	width: 70%;
	height: 43px;
	padding: 10px 15px;
	font-size: 15px;
	line-height: 1.428571429;
	color: #2c3e50;
	vertical-align: middle;
	background-color: #ffffff;
	background-image: none;
	border: 1px solid #dce4ec;
	border-radius: 4px;
	-webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
	box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
	-webkit-transition: border-color ease-in-out 0.15s, box-shadow
		ease-in-out 0.15s;
	transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s;
}

.btn {
	display: inline-block;
	padding: 10px 15px;
	margin-bottom: 0;
	font-size: 15px;
	font-weight: normal;
	line-height: 1.428571429;
	text-align: center;
	white-space: nowrap;
	vertical-align: middle;
	cursor: pointer;
	background-image: none;
	border: 1px solid transparent;
	border-radius: 4px;
}

.btn-warning {
	color: #ffffff;
	background-color: #ff6363;
	border-color: #ff6363;
}

.btn-default {
	color: #ffffff;
	background-color: #bbbbbb;
	border-color: #bbbbbb;
}
</style>
</head>
<%@ include file="/views/common.jsp"%>
<body>

	<div align="center">
		<form action="${pageContext.request.contextPath}/customer/modify.do"
			method="POST">
			<table class="table" align="center">
				<tr>
					<th>아이디</th>
					<td>${user.customerId}</td>
				</tr>
				<tr>
					<th>비밀번호</th>
					<td><input class="form-control" type="password"
						id="customerPass" name="customerPass" placeholder="비밀번호" value = "${user.customerPass}"></td>
				</tr>
				<tr>
					<th>이름</th>
					<td><input class="form-control" type="text" id="customerName"
						name="customerName" value="${user.customerName}"></td>
				</tr>
				<tr>
					<th>나이</th>
					<td><input class="form-control" type="text" id="customerAge"
						name="customerAge" placeholder="이메일" value="${user.customerAge}"></td>
				</tr>
				<tr>
					<th>성별</th>
					<td><select name="customerSex"
						style="border: 3px solid #dce4ec;" value="${user.customerSex}">
							<option value="남성">남성</option>
							<option value="여성">여성</option>
					</select></td>
				</tr>
				<tr>
					<th>전화번호</th>
					<td><input class="form-control" type="text"
						id="customerNumber" name="customerNumber" placeholder="전화번호" value="${user. customerNumber}"></td>

				</tr>
				<tr>
					<th>주민번호</th>
					<td><input class="form-control" type="text"
						id="customerSSNumber" name="customerSSNumber" placeholder="주민번호" value="${user. customerSSNumber}">
					</td>
				</tr>
				<tr>
					<th>여권번호</th>
					<td><input class="form-control" type="text"
						id="passportNumber" name="passportNumber" placeholder="여권번호" value="${user. passportNumber}"></td>
				</tr>
				<tr>
					<th>주소</th>
					<td><input class="form-control" type="text" id="area"
						name="customeraddr" placeholder="주소" value="${user. customeraddr}"></td>
				</tr>	

			</table>
			<div align="center">
				<button class="btn" type="submit">수정</button>
			</div>

		</form>
	</div>
</body>
</html>