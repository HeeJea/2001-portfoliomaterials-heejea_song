 <%@ page language="java" contentType="text/html; charset=euc-kr" pageEncoding="euc-kr" import="java.sql.*"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<!--해당 문서의 DTD 연결한다.-->
<html>
	<head>
		<meta charset="EUC-KR">
		<title>DB값 불러오기</title>
	</head>
	<body bgcolor="#EAEAEA">
	<h2 align="center">회원 정보 리스트(DB값 불러오기)</h2>
	<hr>
<!--해당 문서 내 한글 폰트 손상 관리한다.-->
<%
	Connection conn = null;
	PreparedStatement pstmt = null;	
	// DB와 연결된 커녁션을 생성 후 필요시에만 임시로 빌려서 사용 후 반환하는 방식이다.
	//DB 연결로부터 SQL을 실행이 가능하도록 하는 메소드이며 SQL문을 미리 생성 후 변수를 입력받는 방식이다.

	String jdbc_driver = "com.mysql.jdbc.Driver";
	String jdbc_url = "jdbc:mysql://localhost:3306/xml";
	 // 자바에서 DB로 접근하는 정보 변수이다.
	 // 해당 DB에 대한 경로이다.
	try{
		Class.forName(jdbc_driver);		
		// DB 드라이버를 확인한다.
		conn = DriverManager.getConnection(jdbc_url,"root","1234");
		// 해당 DB에 접근한다.
		String sql = "select * from nsfml order by 1 asc";
		// SQL구문을 작성한다. 1울 기준으로 오름차순 정렬하여 모든 결과값을 출력한다.
		pstmt = conn.prepareStatement(sql);
		// 연결한 DB에 해단 sql문을 입력한다.

		ResultSet rs = pstmt.executeQuery();
		 // sql문이 실행된 값이 rs 객체로 반환된다.
		int i=1;
		// i를 1로 초기화한다. 즉 i값이 증가되면서 순서대로 출력하기 때문이다.
		


%>
	<table border=1 align="center">
		<tr>
			<th bgcolor="#FAED71">No</th>
			<th bgcolor="#FAED71">아이디</th>
			<th bgcolor="#FAED71">비밀번호</th>
			<th bgcolor="#FAED71">이름</th>
			<th bgcolor="#FAED71">전화번호</th>
			<th bgcolor="#FAED71">이메일</th>
			<th bgcolor="#FAED71">관리자</th>

		</tr>
<%	while(rs.next()) // rs.next()를 이용하여 값이 있을 때 까지 반복을 돌린다. 
	{
			%>
			<tr>
				<td bgcolor="#EEEEEE"><%=i%></td>
				<td bgcolor="#EEEEEE" width=200><%=rs.getString("id")%></td>
				<td bgcolor="#EEEEEE" width=300 align=center><%=rs.getString("pw")%></td>
				<td bgcolor="#EEEEEE" width=500 align=center><%=rs.getString("name")%></td>
				<td bgcolor="#EEEEEE" width=300 align=center><%=rs.getString("phone")%></td>
				<td bgcolor="#EEEEEE" width=300 align=center><%=rs.getString("email")%></td>
				<td bgcolor="#EEEEEE" width=300 align=center><%=rs.getString("Master")%></td>
			
			</tr>
<%		i++; //i를 증가시켜준다. 
		}

		rs.close();
		pstmt.close();
		conn.close();
		// sql 실행과 db 연결을 헤지한다. 
	}
	catch(Exception e) {
		System.out.println(e);
		// 에러 발생시 e를 출력한다.
	}
%>
	</table>
	<table border=1 align="center">
	</table>

</body>
</html>
<hr>

<html>
	<head>
		<meta charset="EUC-KR">
	</head>
	<body bgcolor="#EAEAEA">
	<h2 align="center">이북식 식당 정보 리스트(DB값 불러오기)</h2>
	<hr>
<% request.setCharacterEncoding("euc-kr"); %>
<!--해당 문서 내 한글 폰트 손상 관리한다.-->
<%
	String jdbc_driver2 = "com.mysql.jdbc.Driver";
	String jdbc_url2 = "jdbc:mysql://localhost:3306/xml";
	 // 자바에서 DB로 접근하는 정보 변수이다.
	 // 해당 DB에 대한 경로이다.
	try{
		Class.forName(jdbc_driver2);		
		// DB 드라이버를 확인한다.
		conn = DriverManager.getConnection(jdbc_url2,"root","1234");
		// 해당 DB에 접근한다.
		String sql2 = "select * from nkrest order by 1 asc";
		// SQL구문을 작성한다. 1울 기준으로 오름차순 정렬하여 모든 결과값을 출력한다.
		pstmt = conn.prepareStatement(sql2);
		// 연결한 DB에 해단 sql2문을 입력한다.

		ResultSet rs2 = pstmt.executeQuery();
		 // sql2문이 실행된 값이 rs2 객체로 반환된다.
		int i=1;
		// i를 1로 초기화한다. 즉 i값이 증가되면서 순서대로 출력하기 때문이다.



%>
	<table border=1 align="center">
		<tr>
			<th bgcolor="#FAED71">No</th>
			<th bgcolor="#FAED71">이북식 식당 이미지 이름</th>
			<th bgcolor="#FAED71">이북식 식당 이름</th>
			<th bgcolor="#FAED71">이북식 식당 위치</th>
			<th bgcolor="#FAED71">이북식 식당 메뉴</th>
			<th bgcolor="#FAED71">이북식 식당 전화번호</th>
			<th bgcolor="#FAED71">이북식 식당 간단 설명</th>
		</tr>
<%	while(rs2.next()) // rs.next()를 이용하여 값이 있을 때 까지 반복을 돌린다. 
	{
		%>
			<tr>
				<td><%=i%></td>
				<td width=200><%=rs2.getString("North_Korea_img")%></td>
				<td width=200><%=rs2.getString("North_Korea_name")%></td>
				<td width=200><%=rs2.getString("North_Korea_location")%></td>
				<td width=200><%=rs2.getString("North_Korea_menu")%></td>
				<td width=200><%=rs2.getString("North_Korea_number")%></td>
				<td width=200><%=rs2.getString("North_Korea_Explantion")%></td>
			</tr>
			</tr>
<%		i++; //i를 증가시켜준다. 
		}

		rs2.close();
		pstmt.close();
		conn.close();
		// sql 실행과 db 연결을 헤지한다. 
	}
	catch(Exception e) {
		System.out.println(e);
		// 에러 발생시 e를 출력한다.
	}
%>
	</table>
	<table border=1 align="center">
	</table>

</body>
</html>
<hr>

<html>
	<head>
		<meta charset="EUC-KR">
	</head>
	<body bgcolor="#EAEAEA">
	<h2 align="center">레시피 정보 리스트(DB값 불러오기)</h2>
	<hr>
<% request.setCharacterEncoding("euc-kr"); %>
<!--해당 문서 내 한글 폰트 손상 관리한다.-->
<%
	String jdbc_driver3 = "com.mysql.jdbc.Driver";
	String jdbc_url3 = "jdbc:mysql://localhost:3306/xml";
	 // 자바에서 DB로 접근하는 정보 변수이다.
	 // 해당 DB에 대한 경로이다.
	try{
		Class.forName(jdbc_driver3);		
		// DB 드라이버를 확인한다.
		conn = DriverManager.getConnection(jdbc_url3,"root","1234");
		// 해당 DB에 접근한다.
		String sql3 = "select * from nsfrec order by 1 asc";
		// SQL구문을 작성한다. 1울 기준으로 오름차순 정렬하여 모든 결과값을 출력한다.
		pstmt = conn.prepareStatement(sql3);
		// 연결한 DB에 해단 sql3문을 입력한다.

		ResultSet rs3 = pstmt.executeQuery();
		 // sql3문이 실행된 값이 rs3 객체로 반환된다.
		int i=1;
		// i를 1로 초기화한다. 즉 i값이 증가되면서 순서대로 출력하기 때문이다.



%>
	<table border=1 align="center">
		<tr>
			<th bgcolor="#FAED71">No</th>
			<th bgcolor="#FAED71">북한 음식 이름</th>
			<th bgcolor="#FAED71">북한 음식 유래</th>
			<th bgcolor="#FAED71">북한 음식 위치(지역)</th>
			<th bgcolor="#FAED71">북한 음식 재료</th>
			<th bgcolor="#FAED71">북한 음식 레시피</th>
			<th bgcolor="#FAED71">북한 음식 소요 시간</th>
			<th bgcolor="#FAED71">북한 위치 X좌표</th>
			<th bgcolor="#FAED71">북한 위치 Y좌표</th>
			<th bgcolor="#FAED71">이북식 식당 X좌표</th>
			<th bgcolor="#FAED71">이북식 식당 Y좌표</th>
			<th bgcolor="#FAED71">이북식 식당 이름</th>
		</tr>
<%	while(rs3.next()) // rs.next()를 이용하여 값이 있을 때 까지 반복을 돌린다. 
	{
		%>
			<tr>
				<td><%=i%></td>
				<td width=60><%=rs3.getString("name")%></td>
				<td width=200><%=rs3.getString("food_feature")%></td>
				<td width=60><%=rs3.getString("North_location")%></td>
				<td width=200><%=rs3.getString("F_ingredients")%></td>
				<td width=1200><%=rs3.getString("Recipe_order")%></td>
				<td width=60><%=rs3.getString("Recipe_time")%></td>
				<td width=50><%=rs3.getString("North_location_X")%></td>
				<td width=50><%=rs3.getString("North_location_Y")%></td>
				<td width=50><%=rs3.getString("Korea_Rest_X")%></td>
				<td width=50><%=rs3.getString("Korea_Rest_Y")%></td>
				<td width=50><%=rs3.getString("Korea_restaurant")%></td>
			</tr>
			</tr>
<%		i++; //i를 증가시켜준다. 
		}

		rs3.close();
		pstmt.close();
		conn.close();
		// sql 실행과 db 연결을 헤지한다. 
	}
	catch(Exception e) {
		System.out.println(e);
		// 에러 발생시 e를 출력한다.
	}
%>
	</table>
	<table border=1 align="center">
	</table>

</body>
</html>
<input type="button"  value="로그아웃 하러가기" onClick="location.href='logout.jsp'">
<input type="button"  value="돌아가기" onClick="location.href='list.jsp'">
