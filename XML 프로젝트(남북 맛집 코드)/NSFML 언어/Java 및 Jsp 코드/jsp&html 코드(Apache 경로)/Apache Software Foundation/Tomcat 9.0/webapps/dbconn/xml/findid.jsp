<%@ page language="java" contentType="text/html; charset=euc-kr" import="java.sql.*" pageEncoding="euc-kr" %>
<!--문서의 정보를 전달해주는 기능을 한다. contentTtype : 출력 형태를 지정, charset : 응답 결과 출력 인코딩 방식 지정, pageEncoding : 문서 시작 시 문서의 인코딩 방식 지정
import : 해당 문서 내에서 사용 할 기능울 정의한다.-->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<!--해당 문서의 DTD 연결한다.-->
<% request.setCharacterEncoding("euc-kr"); %>

<!--해당 문서 내 한글 폰트 손상 관리헌다.-->
<%
	 Connection conn = null; 
	// DB와 연결된 커녁션을 생성 후 필요시에만 임시로 빌려서 사용 후 반환하는 방식이다.
	 PreparedStatement pstmt = null;
	 //DB 연결로부터 SQL을 실행이 가능하도록 하는 메소드이며 SQL문을 미리 생성 후 변수를 입력받는 방식이다.


	 String jdbc_driver = "com.mysql.jdbc.Driver"; // 자바에서 DB로 접근하는 정보 변수이다.
	 String jdbc_url = "jdbc:mysql://localhost:3306/xml";// 해당 DB에 대한 경로이다.
	 String jname = request.getParameter("fname"); // fname 파라미터값을 jname에 반환한다
	 String jphone = request.getParameter("fphone"); // fphone 파라미터값을 jphone에 반환한다
	 String jemail = request.getParameter("femail"); // femail 파라미터값을 jemail에 반환한다
	 String id = ""; // id 변수는 초기화 시켜준다. 

	try{
		Class.forName(jdbc_driver); // DB 드라이버를 확인한다.
		conn = DriverManager.getConnection(jdbc_url,"root","1234"); // 해당 DB에 접근한다.

		String sql = "select id from nsfml where name = '" + jname + "' AND phone='" + jphone + "' AND email='" + jemail + "'";
		// SQL구문을 작성한다. 입력된 jname, jphone, jemail에 알맞는 id값을 찾는다.
		pstmt = conn.prepareStatement(sql); // 연결한 DB에 해단 sql문을 입력한다.
		ResultSet rs = pstmt.executeQuery(); // sql문이 실행된 값이 rs 객체로 반환된다.
		
		Boolean ID = false; // 부울 변수 ID를 생성한다. 초기 값은 false이다.
		while(rs.next()) // rs.next()를 이용하여 값이 있을 때 까지 반복을 돌린다. 
		{
			id = rs.getString("id"); // id값에 데이터베이스에 있는 id값을 입력한다.
			ID = true; // ID는 true로 변환한다. 즉 값이 있을 경우 true로 바꿔준다. 
		}
		
%>
<html>
   <head>
       <meta charset="EUC-KR">
	   <title> 아이디 찾기 </title> <!--아이디 찾기를 위한 테이블을 만들어준다.-->
   </head>
    <body bgcolor="#EAEAEA">

		<h2 align="center">아이디 찾기</h2>
		<hr>
	    <table bgcolor= "#FFFFFF" border="1" width="280" align="center">
        <tr>
			
			<td width="150" align="left">
				<%
					if(ID) // Id가 참일 경우, 아이디가 있을 경우를 말한다.
					{
					out.print(jname + "의" );	
				%>	아이디 :
				<%
					out.print(id + "입니다.");		
					}
					// 해당 아이디의 값을 출력해준다.
					else
					{
					out.println("<script>");
					out.println("alert('아이디가 존재하지 않습니다.:)')");
					out.println("location.href='findpa.jsp'");
					out.println("</script>");
					}
					// 없을 경우 존재하지 않는다고 출력해준다. 
				%>
			</td>
  		</tr>
		<tr>
			<td width="150" align="center">
		<input type="button" value="로그인 화면으로" onclick="location.href='login.jsp'">
			</td>
		</tr>
		</table>
	</body>
</html>
<%
		rs.close();
		pstmt.close();
		conn.close();
	} // sql문의 실행과 DB 연결 헤제한다.
	catch(Exception e) {
		System.out.println(e);
	}
	//에러 발생시 e값(에러발생한 원인에 대한값을)출력한다.
%>
