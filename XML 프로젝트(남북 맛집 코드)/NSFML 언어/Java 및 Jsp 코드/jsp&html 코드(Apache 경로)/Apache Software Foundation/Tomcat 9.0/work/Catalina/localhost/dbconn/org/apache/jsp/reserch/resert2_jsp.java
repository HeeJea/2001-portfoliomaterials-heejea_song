/*
 * Generated by the Jasper component of Apache Tomcat
 * Version: Apache Tomcat/9.0.13
 * Generated at: 2018-12-11 02:31:25 UTC
 * Note: The last modified time of this file was set to
 *       the last modified time of the source file after
 *       generation to assist with modification tracking.
 */
package org.apache.jsp.reserch;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.sql.*;

public final class resert2_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent,
                 org.apache.jasper.runtime.JspSourceImports {

  private static final javax.servlet.jsp.JspFactory _jspxFactory =
          javax.servlet.jsp.JspFactory.getDefaultFactory();

  private static java.util.Map<java.lang.String,java.lang.Long> _jspx_dependants;

  private static final java.util.Set<java.lang.String> _jspx_imports_packages;

  private static final java.util.Set<java.lang.String> _jspx_imports_classes;

  static {
    _jspx_imports_packages = new java.util.HashSet<>();
    _jspx_imports_packages.add("java.sql");
    _jspx_imports_packages.add("javax.servlet");
    _jspx_imports_packages.add("javax.servlet.http");
    _jspx_imports_packages.add("javax.servlet.jsp");
    _jspx_imports_classes = null;
  }

  private volatile javax.el.ExpressionFactory _el_expressionfactory;
  private volatile org.apache.tomcat.InstanceManager _jsp_instancemanager;

  public java.util.Map<java.lang.String,java.lang.Long> getDependants() {
    return _jspx_dependants;
  }

  public java.util.Set<java.lang.String> getPackageImports() {
    return _jspx_imports_packages;
  }

  public java.util.Set<java.lang.String> getClassImports() {
    return _jspx_imports_classes;
  }

  public javax.el.ExpressionFactory _jsp_getExpressionFactory() {
    if (_el_expressionfactory == null) {
      synchronized (this) {
        if (_el_expressionfactory == null) {
          _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
        }
      }
    }
    return _el_expressionfactory;
  }

  public org.apache.tomcat.InstanceManager _jsp_getInstanceManager() {
    if (_jsp_instancemanager == null) {
      synchronized (this) {
        if (_jsp_instancemanager == null) {
          _jsp_instancemanager = org.apache.jasper.runtime.InstanceManagerFactory.getInstanceManager(getServletConfig());
        }
      }
    }
    return _jsp_instancemanager;
  }

  public void _jspInit() {
  }

  public void _jspDestroy() {
  }

  public void _jspService(final javax.servlet.http.HttpServletRequest request, final javax.servlet.http.HttpServletResponse response)
      throws java.io.IOException, javax.servlet.ServletException {

    if (!javax.servlet.DispatcherType.ERROR.equals(request.getDispatcherType())) {
      final java.lang.String _jspx_method = request.getMethod();
      if ("OPTIONS".equals(_jspx_method)) {
        response.setHeader("Allow","GET, HEAD, POST, OPTIONS");
        return;
      }
      if (!"GET".equals(_jspx_method) && !"POST".equals(_jspx_method) && !"HEAD".equals(_jspx_method)) {
        response.setHeader("Allow","GET, HEAD, POST, OPTIONS");
        response.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED, "JSPs only permit GET, POST or HEAD. Jasper also permits OPTIONS");
        return;
      }
    }

    final javax.servlet.jsp.PageContext pageContext;
    javax.servlet.http.HttpSession session = null;
    final javax.servlet.ServletContext application;
    final javax.servlet.ServletConfig config;
    javax.servlet.jsp.JspWriter out = null;
    final java.lang.Object page = this;
    javax.servlet.jsp.JspWriter _jspx_out = null;
    javax.servlet.jsp.PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html; charset=euc-kr");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n");
      out.write("<!--문서의 정보를 전달해주는 기능을 한다. contentTtype : 출력 형태를 지정, charset : 응답 결과 출력 인코딩 방식 지정, pageEncoding : 문서 시작 시 문서의 인코딩 방식 지정\r\n");
      out.write("import : 해당 문서 내에서 사용 할 기능울 정의한다.-->\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\r\n");
      out.write("\r\n");
      out.write("<!--해당 문서의 DTD 연결한다.-->\r\n");
      out.write("<html>\r\n");
      out.write("\t<head>\r\n");
      out.write("\t\t<meta charset=\"EUC-KR\">\r\n");
      out.write("\t\t<title>설문 정보 관리 리스트</title>\r\n");
      out.write("\t</head>\r\n");
      out.write("\t<body bgcolor=\"#EAEAEA\">\r\n");
      out.write("\t<h2 align=\"center\">설문 리스트</h2>\r\n");
      out.write("\t<hr>\r\n");
 request.setCharacterEncoding("euc-kr"); 
      out.write("\r\n");
      out.write("<!--해당 문서 내 한글 폰트 손상 관리한다.-->\r\n");

	Connection conn = null;
	PreparedStatement pstmt = null;	
	// DB와 연결된 커녁션을 생성 후 필요시에만 임시로 빌려서 사용 후 반환하는 방식이다.
	//DB 연결로부터 SQL을 실행이 가능하도록 하는 메소드이며 SQL문을 미리 생성 후 변수를 입력받는 방식이다.

	String jdbc_driver = "com.mysql.jdbc.Driver";
	String jdbc_url = "jdbc:mysql://localhost:3306/jspdb";
	 // 자바에서 DB로 접근하는 정보 변수이다.
	 // 해당 DB에 대한 경로이다.
	try{
		Class.forName(jdbc_driver);		
		// DB 드라이버를 확인한다.
		conn = DriverManager.getConnection(jdbc_url,"root","1234");
		// 해당 DB에 접근한다.
		String sql = "select uage, count(*) as cnt from researchtbl group by uage ";
		// SQL구문을 작성한다. 1울 기준으로 오름차순 정렬하여 모든 결과값을 출력한다.
		pstmt = conn.prepareStatement(sql);
		// 연결한 DB에 해단 sql문을 입력한다.

		ResultSet rs = pstmt.executeQuery();
		 // sql문이 실행된 값이 rs 객체로 반환된다.
		int i=1;
		// i를 1로 초기화한다. 즉 i값이 증가되면서 순서대로 출력하기 때문이다.
		Boolean check = false;
		// check라는 부울변수를 생성하고 초기값은 false를 준다.



      out.write("\r\n");
      out.write("\t<table border=1 align=\"center\">\r\n");
      out.write("\t\t<tr>\r\n");
      out.write("\t\t\t<th bgcolor=\"#FAED71\">No</th>\r\n");
      out.write("\t\t\t<th bgcolor=\"#FAED71\">연령</th>\r\n");
      out.write("\t\t\t<th bgcolor=\"#FAED71\">count</th>\t\t\t\r\n");
      out.write("\t\t</tr>\r\n");
	while(rs.next()) // rs.next()를 이용하여 값이 있을 때 까지 반복을 돌린다. 
	{
		check = true; // check가 참일 경우 해당 테이블을 생성한다.
			
      out.write("\r\n");
      out.write("\t\t\t<tr>\r\n");
      out.write("\t\t\t\t<td align=center>");
      out.print(i);
      out.write("</td>\r\n");
      out.write("\t\t\t\t<td bgcolor=\"#FFA2A2\" width=200 align=center>");
      out.print(rs.getInt("uage"));
      out.write("</td>\r\n");
      out.write("\t\t\t\t<td bgcolor=\"#FFA2A2\" width=200 align=center>");
      out.print(rs.getInt("cnt"));
      out.write("</td>\r\n");
      out.write("\t\t\t</tr>\r\n");
		i++; //i를 증가시켜준다. 
		}
		if(!check) // check가 false일때 즉 회원정보가 없을 경우 경고 스크립트를 알려주고 신규회원가입 창으로 넘어간다. 
		{
			out.println("<script>");
			out.println("alert('회원 목록이 없습니다. 신규회원가입 창으로 넘어갑니다.')");
			out.println("location.href='form1.html'");
			out.println("</script>");;
		}


		rs.close();
		pstmt.close();
		conn.close();
		// sql 실행과 db 연결을 헤지한다. 
	}
	catch(Exception e) {
		System.out.println(e);
		// 에러 발생시 e를 출력한다.
	}

      out.write("\r\n");
      out.write("\t</table><!--경로 이동을 위한 테이블을 생성한다. -->\r\n");
      out.write("\t<table border=1 align=\"center\">\r\n");
      out.write("\t<tr>\r\n");
      out.write("\t\t<td >\r\n");
      out.write("\t\t<input type=\"button\"  value=\"설문하러 가기 하러가기\" onClick=\"location.href='input.html'\">\r\n");
      out.write("\t\t</td>\r\n");
      out.write("\t</tr>\r\n");
      out.write("\t</table>\r\n");
      out.write("\r\n");
      out.write("</body>\r\n");
      out.write("</html>\r\n");
    } catch (java.lang.Throwable t) {
      if (!(t instanceof javax.servlet.jsp.SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try {
            if (response.isCommitted()) {
              out.flush();
            } else {
              out.clearBuffer();
            }
          } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
