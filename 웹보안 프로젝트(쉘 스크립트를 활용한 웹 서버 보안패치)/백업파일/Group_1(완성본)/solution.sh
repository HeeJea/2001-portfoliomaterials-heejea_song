#! /bin/bash
sudo echo "<?
function sql_filter("'$input'") {
"'$input'" = str_replace("'"'"'"'"'",'&apos;',"'$input'"); 
"'$input'" = str_replace('"'"'"','&quot;',"'$input'");
"'$input'" = str_replace('-','&#x2D;',"'$input'");
"'$input'" = str_replace('=','&equals;',"'$input'");
"'$input'" = str_replace('(','&lpar;',"'$input'");
"'$input'" = str_replace(')','&rpar;',"'$input'");
"'$input'" = str_replace('/','&sol;',"'$input'");
"'$input'" = str_replace('*','&ast;',"'$input'");
"'$input'" = str_replace('+','&plus;',"'$input'");
"'$input'" = str_replace('<','&lt;',"'$input'");
"'$input'" = str_replace('>','&gt;',"'$input'");
"'$input'" = str_replace('&&','&amp;&amp;',"'$input'");
"'$input'" = str_replace('|','&vert;',"'$input'");
"'$input'" = str_replace('%','&percnt;',"'$input'");

"'$input'" = str_replace('SELECT','&#x53;&#x45;&#x4C;&#x45;&#x43;&#54;',"'$input'");
"'$input'" = str_replace('INSERT','&#x49;&#x4E;&#x53;&#x45;&#x52;&#54;',"'$input'");
"'$input'" = str_replace('UPDATE','&#x55;&#x50;&#x44;&#x41;&#x54;&#45;',"'$input'");
"'$input'" = str_replace('DELETE','&#x44;&#x45;&#x4C;&#x45;&#x54;&#45;',"'$input'");

"'$input'" = str_replace('UNION','&#x55;&#x4E;&#x49;&#x4F;&#x4E;',"'$input'");
"'$input'" = str_replace('AND','&#x41;&#x4E;&#x44;',"'$input'");
"'$input'" = str_replace('OR','&#x4F;&#x52;',"'$input'");
"'$input'" = str_replace('FROM','&#x46;&#x52;&#x4F;&#x4D;',"'$input'");
"'$input'" = str_replace('WHERE','&#x57;&#x48;&#x45;&#x52;&#x45;',"'$input'");
"'$input'" = str_replace('DROP','&#x44;&#x52;&#x4F;&#x50;',"'$input'");
"'$input'" = str_replace('IF','&#x49;&#x46;',"'$input'");
"'$input'" = str_replace('JOIN','&#x4A;&#x4F;&#x49;&#x4E;',"'$input'");

return "'$input'";

}

?>" >> /var/www/html/filter.php

sudo sed -i '/$no = /d' /var/www/html/read.php;
sudo sed -i '/$id = /d' /var/www/html/read.php;
sudo sed -i 's/#JINWON/include "filter.php";\n#GROUP1\n$no = sql_filter($_GET[no]);\n$id = sql_filter($_GET[id]);/' /var/www/html/read.php;
sudo sed -i 's/<!--leejinwon--><?=/<!--leejinwon--><?=strip_tags/' /var/www/html/read.php

sudo sed -i "s/ServerTokens OS/ServerTokens Prod/g" /etc/apache2/conf-enabled/security.conf
sudo sed -i "s/ServerSignature On/ServerSignature Off/g" /etc/apache2/conf-enabled/security.conf

sudo service apache2 restart
